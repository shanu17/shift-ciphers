import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class break_shift_cipher {

	static Map <Character,Integer> binding1 = new HashMap<Character, Integer>();		// Maps the alphabets (A-Z) to numbers (0-25)
	static Map <Integer,Character> binding2 = new HashMap<Integer, Character>();		// Maps the numbers (0-25) to alphabets (A-Z)
	
	public static void main(String[] args) {
		
		bind();
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the ciphertext to crack:");
		String cipher = input.nextLine().toUpperCase();
		for(int key=1;key<26;key++)		// Runs decrypt for every key (0-25)
		{
			String plaintext = decrypt(cipher,key);
			System.out.println("For key " + key + " plaintext is :" + plaintext);
		}
		input.close();
	}
	
	private static void bind()
	{
		int x = 0;
		for(char i='A';i<='Z';i++)
		{
			binding1.put(i,x);
			binding2.put(x,i);
			x++;
		}
	}
	private static String decrypt(String text, int key) {
		String result = "";
		
		for (int i=0; i<text.length(); i++) 
		{
			if(!binding1.containsKey(text.charAt(i)))
			{
				result+=text.charAt(i);
			}
			else if((binding1.get(text.charAt(i)) - key)%26 < 0 )
			{
				char ch = binding2.get(((binding1.get(text.charAt(i)) - key)%26)+26);
				result+=ch;
			}
			else
			{
				char ch = binding2.get((binding1.get(text.charAt(i)) - key)%26);
				result+=ch;
			}	
		}
		return result;
	}
}
