import java.util.*;

public class shift_cipher {

	static Map <Character,Integer> binding1 = new HashMap<Character, Integer>();	// Maps the alphabets (A-Z) to numbers (0-25)
	static Map <Integer,Character> binding2 = new HashMap<Integer, Character>();	// Maps the numbers (0-25) to alphabets (A-Z)
	public static void main(String[] args) {
		
		bind();		// Creates the actual bindings
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the text to encrypt or decrypt:");
		String text = input.nextLine().toUpperCase();
		System.out.println("Enter E for Encryption or D for Decryption");
		char c = input.next().toUpperCase().charAt(0);
		System.out.println("Enter the Key to use:");
		char key = input.next().toUpperCase().charAt(0);
		switch(c)
		{
		case 'E':
			String output = encrypt(text,key);
			if(output != null)
				System.out.println("The encrypted text is :" + output);
			else
				System.out.println("Key is not a character");
			break;
		case 'D':
			String output1 = decrypt(text,key);
			if(output1 != null)
				System.out.println("The decrypted text is :" + output1);
			else
				System.out.println("Key is not a character");
			break;
		default:
			System.out.println("Must Input E or D");
			break;
		}
		input.close();
	}

	private static String decrypt(String text, char key) {
		if(binding1.containsKey(key))
		{
			String result = "";
			int k = binding1.get(key);		// Gets the integer value of the Key
			for (int i=0; i<text.length(); i++)		
			{
				if(!binding1.containsKey(text.charAt(i)))	// Checks if it is a letter if not ignore it 
				{
					result+=text.charAt(i);
				}
				else if((binding1.get(text.charAt(i)) - k)%26 <0 )		// If value is less than zero add 26
				{
					char ch = binding2.get(((binding1.get(text.charAt(i)) - k)%26)+26);
					result+=ch;
				}
				else
				{
					char ch = binding2.get((binding1.get(text.charAt(i)) - k)%26);
					result+=ch;
				}	
			}
			return result;
		}
		else
		{
			return null;
		}		
	}

	private static String encrypt(String text, char key) {
		if(binding1.containsKey(key))
		{
			String result = "";
			int k = binding1.get(key);
			for (int i=0; i<text.length(); i++) 
			{
				if(binding1.containsKey(text.charAt(i)))
				{
					char ch = binding2.get((binding1.get(text.charAt(i)) + k)%26);
					result+=ch;
				}
				else
				{
					result+=text.charAt(i);
				}
				
			}
			return result;
		}
		else
		{
			return null;
		}
	}
	private static void bind()		// Function to produce the mapings 
	{
		int x = 0;
		for(char i='A';i<='Z';i++)
		{
			binding1.put(i,x);
			binding2.put(x,i);
			x++;
		}
	}
}
