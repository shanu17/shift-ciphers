import java.io.*; 
import java.util.*;

public class frequency_analysis {

	public static void main(String[] args) {
		String s = "A heavy step was heard upon the stairs, and an instant later there entered a tall, ruddy, clean-shaven gentleman, whose clear eyes and florid cheeks told of a life led far from the fogs of Baker Street. He seemed to bring a whiff of his strong, fresh, bracing, east-coast air with him as he entered. Having shaken hands with each of us, he was about to sit down when his eye rested upon the paper with the curious markings, which I had just examined and left upon the table.";
		s = s.replaceAll("[^a-zA-Z0-9]", "");
		s = s.trim().toUpperCase();
		characterCount(s);
	}
    static void characterCount(String inputString) 
    { 
        HashMap<Character, Integer> charCountMap = new HashMap<Character, Integer>(); 

        char[] strArray = inputString.toCharArray(); 

        for (char c : strArray) { 
            if (charCountMap.containsKey(c)) { 
                charCountMap.put(c, charCountMap.get(c) + 1); 
            } 
            else { 
                charCountMap.put(c, 1); 
            } 
        } 
  
        // Printing the charCountMap 
        for (Map.Entry entry : charCountMap.entrySet()) { 
            System.out.println(entry.getKey() + " = " + entry.getValue()); 
        } 
    } 
}
